package HomeWork1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Numbers {


    public static void main(String[] args) throws IOException {
        System.out.println("Let the game begin!");
        System.out.println("Enter your name!");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        int rundomNumber = 0 + (int) (Math.random()*100);
        int[] numbers = new int[0];

        while (true){
            int userNumber;
            String userString = reader.readLine();
            if (isInteger(userString) == true){
                userNumber = Integer.parseInt(userString);
                if (userNumber >= 0 && userNumber <= 100) {
                    numbers = incriseArr(numbers, userNumber);
                    if (userNumber > rundomNumber) System.out.println("Your number is too big. Please, try again.");
                    if (userNumber < rundomNumber) System.out.println("Your number is too small. Please, try again.");
                    if (userNumber == rundomNumber){
                        System.out.println("Congratulations, " + name + "!");
                        System.out.println("Your numbers: " + stringOfNumber(numbers));
                        break;
                    }
                } else {
                    System.out.println("Введите целочисленное число от 0 до 100");
                }

            } else {
                System.out.println("Введите целочисленное число от 0 до 100");
            }
        }
    }

    public static int[] incriseArr(int[] arr, int userNumber){
        int[] broaderArr = new int[arr.length+1];
        for (int i = 0; i < arr.length; i++){
            broaderArr[i] = arr[i];
        }
        broaderArr[broaderArr.length-1] = userNumber;
        return broaderArr;
    }

    public static String stringOfNumber(int[] arr){
        String s = "";
        Arrays.sort(arr);
        for (int i = arr.length-1; i >= 0; i--){
            s = s + " " + arr[i];
        }

        return s;
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }
}
